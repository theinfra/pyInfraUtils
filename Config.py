'''
Created on May 4, 2017

@author: infra
'''
import ConfigParser, os

class pIUConfig(object):
    '''
    classdocs
    '''
    instance = object()
    
    def parseConfigFile(self, configfilename = os.getcwd()+'/config.ini'):
        pIUConfigObject = ConfigParser.SafeConfigParser()
        
        configFile = configfilename
        parsedConfigFile = pIUConfigObject.read(configFile)
        return pIUConfigObject
    
    def cfg(self, section, key):
        val = None
        try:
            val = self.instance.get(section, key)
        except BaseException as e:
            # corregir cuando se busca la informacion de output del config
            try:
                p
            except NameError:
                print "Ocurrio un error al buscar el valor %s en la seccion de configuracion %s: %s" % (key, section, str(e))
            else:
                p.print_error("Ocurrio un error al buscar el valor %s en la seccion de configuracion %s: %s" % (key, section, str(e)))
            return ""
            
        return val

    def __init__(self, configfilename = os.getcwd()+'/config.ini'):
         self.instance = self.parseConfigFile(configfilename)