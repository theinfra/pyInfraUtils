'''
Created on May 5, 2017

@author: infra
'''
import sys, pprint, logging
from time import gmtime, strftime
from pyInfraUtils.Config import pIUConfig
import LogPrinterBase

c = pIUConfig()

pp = pprint.PrettyPrinter(indent=4)

logger = logging.getLogger('service')

class LogFilePrinterClass(LogPrinterBase.LogPrinterClassBase):

    debugmode = None

    def __init__(self):
        LogPrinterBase.LogPrinterClassBase.__init__(self)
        if(self.debugmode == True):
            logging.basicConfig(filename='log/service.txt',level=logging.DEBUG)
            logger.setLevel(10)
        else:
            logging.basicConfig(filename='log/service.txt',level=logging.INFO)
            logger.setLevel(20)

    def print_formated(self, msg, severity, quitapp = False):
        
        try:
            if(type(msg) in self.datatypes):
                data = msg
                msg = str(type(data))
            else:
                data = None
        except:
            data = None
            
        msgtime = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        if(severity == 'error'):
            logger.error('%s [ERROR] %s' % (msgtime, msg))
        elif(severity == 'success'):
            logger.info('%s [SUCCESS] %s' % (msgtime, msg))
        elif(severity == 'warning'):
            logger.warning('%s [WARN] %s' % (msgtime, msg))
        elif(severity == 'info'):
            logger.info('%s [INFO] %s' % (msgtime, msg))
        elif(severity == 'debug'):
            logger.debug('%s [DEBUG] %s' % (msgtime, msg))
        
        if(data != None):
            if(severity == 'error'):
                logger.error('%s [ERROR] %s' % (msgtime, pp.pformat(data)))
            elif(severity == 'success'):
                logger.info('%s [SUCCESS] %s' % (msgtime, pp.pformat(data)))
            elif(severity == 'warning'):
                logger.warning('%s [WARN] %s' % (msgtime, pp.pformat(data)))
            elif(severity == 'info'):
                logger.info('%s [INFO] %s' % (msgtime, pp.pformat(data)))
            elif(severity == 'debug'):
                logger.debug('%s [DEBUG] %s' % (msgtime, pp.pformat(data)))
            
        if(quitapp == True):
            sys.exit()

if __name__ == '__main__':
    pass