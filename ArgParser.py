'''
Created on May 4, 2017

@author: infra
'''
import argparse, pprint

pp = pprint.PrettyPrinter(indent=4)

class pIUArgParser(object):
    '''
    classdocs
    '''
    args = {}
    parser = None
    description = ''
    argument_list = []
    
    def parseArgs(self):
        self.parser = argparse.ArgumentParser(description=self.description)
        for item in self.argument_list:
            self.parser.add_argument(item['name'], 
                item['short'],
                help=item['description'],
                nargs=item['nargs'])
        self.parser.add_argument('--debug', help='Activar modo debug', nargs='?', default=False)
        self.parser.add_argument('--dryrun', help='Dry run (solo mensajes no modificar nada', nargs='?', default=False)
        
        args = vars(self.parser.parse_args())
        for argname, value in args.iteritems():
            if(type(value) == list):
                args[argname] = value.pop()

        self.args = args

        return args
    
    def arg(self, key):
        if(key in self.args):
            return self.args[key]
        else:
            return None

    def args(self):
        return self.args

    def setDescription(self, description):
        self.description = description

    def add_argument(self, name, short, description, nargs):
        newitem = {
            'name': name,
            'short': short,
            'description': description,
            'nargs': nargs
            }
        self.argument_list.append(newitem)

    def __init__(self):
        return