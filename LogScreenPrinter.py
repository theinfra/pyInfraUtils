'''
Created on May 5, 2017

@author: infra
'''
import sys, pprint

from pyInfraUtils.Config import pIUConfig
import LogPrinterBase

c = pIUConfig()

from colorama import init, Fore, Back, Style

#iniciar colorama
init()

pp = pprint.PrettyPrinter(indent=4)

class LogScreenPrinterClass(LogPrinterBase.LogPrinterClassBase):

    def __init__(self):
        LogPrinterBase.LogPrinterClassBase.__init__(self)

    def print_formated(self, msg, severity, quitapp = False):
        try:
            if(type(self.datatypes.index(type(msg))) == int):
                data = msg
                msg = str(type(data))
            else:
                data = None
        except:
            data = None

        output = Back.WHITE + '%s[%s]' + Style.RESET_ALL + ' %s'

        if(severity == 'error'):
            output = output % (Fore.RED, 'ERROR', msg)
        elif(severity == 'success'):
            output = output % (Fore.GREEN, 'SUCCESS', msg)
        elif(severity == 'info'):
            output = output % (Fore.CYAN, 'INFO', msg)
        elif(severity == 'debug'):
            output = output % (Fore.YELLOW, 'DEBUG', msg)
            
        if(severity != 'debug' or self.debugmode == True):
            print(output)
            
        if(data != None):
            if(severity == 'debug'):
                if(self.debugmode == True):
                    pp.pprint(data)
            else:
                pp.pprint(data)
            
        if(quitapp == True):
            sys.exit()

if __name__ == '__main__':
    pass