'''
Created on Dec 7, 2017

@author: infra
'''
import sys
from pyInfraUtils.Config import pIUConfig
c = pIUConfig()
from pyInfraUtils import ArgParser
ap = ArgParser.pIUArgParser()
ap.parseArgs()

class LogPrinterClassBase():

	debugmode = None
	datatypes = [list, dict, tuple]

	def __init__(self):
		if(ap.arg("debug") != False):
			self.debugmode = True
		else:
			if(c.cfg("General", "loglevel") == 'debug'):
				self.debugmode = True
			else:
				self.debugmode = False

	def print_formated(self, msg, severity, quitapp = False):
		print "ERROR print_formated llamado con severidad %s sin heredar" % (severity)
		print msg

	def print_debug(self, msg, quitapp = False):
		self.print_formated(msg, 'debug', quitapp)

	def print_info(self, msg, quitapp = False):
		self.print_formated(msg, 'info', quitapp)    

	def print_success(self, msg, quitapp = False):
		self.print_formated(msg, 'success', quitapp)
        
	def print_error(self, msg, quitapp = False):
		self.print_formated(msg, 'error', quitapp)

	def pd(self, msg, quitapp = False):
	    self.print_debug(msg, quitapp)

	def pi(self, msg, quitapp = False):
	    self.print_info(msg, quitapp)    

	def ps(self, msg, quitapp = False):
	    self.print_success(msg, quitapp)
	    
	def pe(self, msg, quitapp = False):
	    self.print_error(msg, quitapp)