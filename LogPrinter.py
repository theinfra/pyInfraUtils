'''
Created on Dec 7, 2017

@author: infra
'''
from pyInfraUtils.Config import pIUConfig
c = pIUConfig()

if (c.cfg("General", "output") == 'screen'):
	import LogScreenPrinter
	p = LogScreenPrinter.LogScreenPrinterClass()
elif (c.cfg("General", "output") == 'log'):
    import LogFilePrinter
    p = LogFilePrinter.LogFilePrinterClass()
else:
	import LogScreenPrinter
	p = LogScreenPrinter.LogScreenPrinterClass()


if __name__ == '__main__':
    pass