'''
Created on Apr 25, 2017

@author: infra
'''

import os, string, random

def ScanDirectory(path, options = {'recurse': True, 'filetypes': '*'}):
    if(path[-1:]) != '/':
        path = path+"/"

    if(os.path.exists(path) == False):
        return False
    
    files = {}
    
    for onefile in os.listdir(path):
        
        newfile = {
                'fullpath': path+onefile,
                'fullname': onefile,
                'name': os.path.splitext(onefile)[0],
                'ext': os.path.splitext(onefile)[1][1:],
                'isdir': os.path.isdir(path+"/"+onefile)
            }
        files[onefile] = newfile
        
    return files

def random_char(y):
       return ''.join(random.choice(string.ascii_letters) for x in range(y))